package com.measure.model;

public class RespMeasureByYear extends Response {

	private String year;
	private Integer total;
	
	public RespMeasureByYear(String errCode, String errMessage) {
		this.errCode = errCode;
		this.errMessage = errMessage;
	}
	
	public RespMeasureByYear(String year, Integer total, String errorCode, String errorMessage) {
		super();
		this.year = year;
		this.total = total;
		this.errCode = errorCode;
		this.errMessage = errorMessage;
	}
	public RespMeasureByYear() {
		super();
	}
	
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	
}
