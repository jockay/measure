package com.measure.util;

public class ErrorConstants {

	public static final String CODE_OK = "0";
	public static final String MESSAGE_OK = "OK";
	
	public static final String CODE_NUMBER_PARSE_ERROR = "-1";
	public static final String MESSAGE_NUMBER_PARSE_ERROR = "The value must be integer (NumberFormatException)."; 
	
	public static final String CODE_DATE_PARSE_ERROR = "-2";
	public static final String MESSAGE_DATE_PARSE_ERROR = "The date pattern must be yyyy-MM-ddTHH-mm-ss (ParseException).";
	
	public static final String CODE_ADDRESS_IN_USE = "-500";
	public static final String MESSAGE_ADDRESS_IN_USE = "CLIENT ADDRESS ALREADY IN USE";
	
	public static final String CODE_UNKOWN_ERROR = "-999";
	public static final String MESSAGE_UNKOWN_ERROR = "Unknown error.";
	
}
