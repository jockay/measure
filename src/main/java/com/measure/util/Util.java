package com.measure.util;

public class Util {

	public static boolean isEmptyString(String s) {
		return s == null || "".equals(s); 
	}
	
	public static boolean notEmptyString(String s) {
		return ! isEmptyString(s);
	}
	
}
