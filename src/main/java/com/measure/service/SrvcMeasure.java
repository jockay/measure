package com.measure.service;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.PathVariable;

import com.measure.entities.Measure;
import com.measure.model.RespMeasureByYear;
import com.measure.model.RespMeasureCountForYear;
import com.measure.model.RespMeasureForYearByMonths;

public interface SrvcMeasure {

	public List<Measure> getAllMeasures();
	
	public List<Measure> getMeasuresByDateInterval(Date dateFrom, Date dateTo);
	
	public RespMeasureByYear getSumOfMeasurementsForYear(HttpServletRequest request, String deviceId, String year);
	public String getSumOfMeasurementsForYearStr(HttpServletRequest request, String deviceId, String year);
	
	public RespMeasureForYearByMonths getMeasurementsForYearByMonths(String year, String deviceId, HttpServletRequest request);
	public String getMeasurementsForYearByMonthsStr(String year, String deviceId, HttpServletRequest request);
	
	public RespMeasureCountForYear getMeasurementCountForYear(String year, String deviceId, HttpServletRequest request);
	public String getMeasurementCountForYearStr(String year, String deviceId, HttpServletRequest request);

	public String addMeasurement(String date, String value, String deviceId, HttpServletRequest request);

	public boolean checkAndManageClient(HttpServletRequest request, String deviceId);
	
	public String removeClientIpAndDeviceId(String ip, String deviceId);
 	
	public String getMyDeviceId(HttpServletRequest request);
	
}
