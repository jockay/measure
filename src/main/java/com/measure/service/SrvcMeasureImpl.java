package com.measure.service;

import static com.measure.util.ErrorConstants.*;

import java.sql.Timestamp;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import com.measure.entities.Client;
import com.measure.entities.Measure;
import com.measure.model.RespMeasureByYear;
import com.measure.model.RespMeasureCountForYear;
import com.measure.model.RespMeasureForYearByMonths;
import com.measure.repositories.RepoClients;
import com.measure.repositories.RepoMeasure;
import com.measure.util.Util;

@org.springframework.stereotype.Service
public class SrvcMeasureImpl implements SrvcMeasure {

	@Autowired
	public RepoMeasure repoMeasure;
	
	@Autowired
	public RepoClients repoClients;
	
	public String[] monthNames;
	
	@Autowired
	public void initMonthNames() {
		DateFormatSymbols dfs = new DateFormatSymbols(Locale.ENGLISH);
        this.monthNames = dfs.getMonths();
	}
	
	@Override
	public List<Measure> getAllMeasures() {
		return repoMeasure.findAll();
	}
	
	@Override
	public List<Measure> getMeasuresByDateInterval(Date dateFrom, Date dateTo) {
//		return null;
		return repoMeasure.find(dateFrom, dateTo);
	}
	
	public List<Measure> getMeasuresForYear(String year) {
		try {
			String strDateTo = year + "-12-31T23-59-59";
			return repoMeasure.find(new SimpleDateFormat("yyyy").parse(year), new SimpleDateFormat("yyyy-MM-dd'T'HH-mm-ss").parse(strDateTo));
		} catch(Exception e) {
			e.printStackTrace();
			return new ArrayList<Measure>();
		}
	}
	
	@Override
	public RespMeasureByYear getSumOfMeasurementsForYear(HttpServletRequest request, String deviceId, String year) {
		try {
			if( ! checkAndManageClient(request, deviceId)) {
				return new RespMeasureByYear(CODE_ADDRESS_IN_USE, MESSAGE_ADDRESS_IN_USE);
			}
			List<Measure> response = getMeasuresForYear(year);
			return new RespMeasureByYear(year, sumMeasureValues(response), CODE_OK, MESSAGE_OK);
		} catch(Exception e) {
			e.printStackTrace();
			return new RespMeasureByYear();
		}
		
	}
	
	
	@Override
	public String getSumOfMeasurementsForYearStr(HttpServletRequest request, String deviceId, String year) {
		RespMeasureByYear response = getSumOfMeasurementsForYear(request, deviceId, year);
		if( Util.notEmptyString(response.getErrCode()) && ! CODE_OK.equals(response.getErrCode())) {
			return "ErrorCode: " + response.getErrCode() + " ErrorMessage: " + response.getErrMessage();
		}
		return "year = " + response.getYear() + ", total = " + response.getTotal();
	}
	
	public Integer sumMeasureValues(List<Measure> list) {
		Integer result = 0;
		for(Measure m : list) {
			result += m.getMeasureValue();
		}
		return result;
	}
	
	@Override
	public RespMeasureForYearByMonths getMeasurementsForYearByMonths(String year, String deviceId, HttpServletRequest request) {
		try {
			if(! checkAndManageClient(request, deviceId)) {
				return new RespMeasureForYearByMonths(CODE_ADDRESS_IN_USE, MESSAGE_ADDRESS_IN_USE);
			}
			RespMeasureForYearByMonths result = new RespMeasureForYearByMonths();
			result.setYear(year);
			List<Measure> response = getMeasuresForYear(year);
			for(int i = 1; i <= 12; i++) {
				List<Measure> monthMeasures = getMeasuresForMonth(response, year, i);
				Integer sumOfMonthMeasures = sumMeasureValues(monthMeasures);
				result.getMonthlyValues().put(monthNames[i-1], sumOfMonthMeasures);
			}
			result.setErrCode(CODE_OK);
			result.setErrMessage(MESSAGE_OK);
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			return new RespMeasureForYearByMonths();
		}
	}
	
	@Override
	public String getMeasurementsForYearByMonthsStr(String year, String deviceId, HttpServletRequest request) {
		RespMeasureForYearByMonths response = getMeasurementsForYearByMonths(year, deviceId, request);
		
		if( Util.notEmptyString(response.getErrCode()) && ! CODE_OK.equals(response.getErrCode())) {
			return "ErrorCode: " + response.getErrCode() + " ErrorMessage: " + response.getErrMessage();
		}
		
		StringBuilder result = new StringBuilder("");
		result.append("year = " + response.getYear());
		for(String key : response.getMonthlyValues().keySet()) {
			result.append(", " +  key + " = " + response.getMonthlyValues().get(key));
		}
		
		return result.toString();
	}
	
	public List<Measure> getMeasuresForMonth(List<Measure> measures, String year, Integer monthNumber) {
		try {
			List<Measure> result = new ArrayList<>();
			String month = (monthNumber < 10) ? "0" + monthNumber : String.valueOf(monthNumber);
			String nextMonth = ((monthNumber + 1) < 10) ? "0" + (monthNumber + 1) : String.valueOf(monthNumber + 1);
			String strDateFrom = year + "-" + month + "-01";
			String strDateTo = year + "-" + nextMonth + "-01";
			if(monthNumber == 12) {
				strDateTo = year + "-12-31";
			}
			Date dateFrom = new SimpleDateFormat("yyyy-MM-dd").parse(strDateFrom);
			Date dateTo = new SimpleDateFormat("yyyy-MM-dd").parse(strDateTo);
			for(Measure m : measures) {
				Date date = m.getMeasureDate();
				if(date.after(dateFrom) && date.before(dateTo)) {
					result.add(m);
				}
//				measures.remove(m);
			}
			return result;
		} catch(Exception e) {
			e.printStackTrace();
			return new ArrayList<Measure>();
		}
	}

	@Override
	public RespMeasureCountForYear getMeasurementCountForYear(String year, String deviceId, HttpServletRequest request) {
		if(! checkAndManageClient(request, deviceId)) {
			return new RespMeasureCountForYear(CODE_ADDRESS_IN_USE, MESSAGE_ADDRESS_IN_USE);
		}
		RespMeasureCountForYear result = new RespMeasureCountForYear();
		List<Measure> response = getMeasuresForYear(year);
		result.setYear(year);
		result.setCount(response.size());
		result.setErrCode(CODE_OK);
		result.setErrMessage(MESSAGE_OK);
		return result;
	}
	
	@Override
	public String getMeasurementCountForYearStr(String year, String deviceId, HttpServletRequest request) {
		RespMeasureCountForYear response = getMeasurementCountForYear(year, deviceId, request);
		if(Util.notEmptyString(response.getErrCode()) && ! CODE_OK.equals(response.getErrCode())) {
			return "ErrorCode: " + response.getErrCode() + " ErrorMessage: " + response.getErrMessage();
		}
		return "year = " + response.getYear() + ", total = " + response.getCount();
	}

	@Override
	public String addMeasurement(String date, String value, String deviceId, HttpServletRequest request) {
		try {
			if(! checkAndManageClient(request, deviceId)) {
				return "ErrorCode: " + CODE_ADDRESS_IN_USE + " ErrorMessage: " + MESSAGE_ADDRESS_IN_USE;
			}
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH-mm-ss");
			Measure measure = new Measure();
			measure.setId(measure.getId());
			measure.setMeasureDate(formatter.parse(date));
			measure.setMeasureValue(Integer.valueOf(value));
			repoMeasure.save(measure);
			repoMeasure.flush();
			return "ErrorCode: " + CODE_OK + " ErrorMessage: " + MESSAGE_OK;
		} catch(NumberFormatException e) {
			e.printStackTrace();
			return "ErrorCode: " + CODE_NUMBER_PARSE_ERROR + " ErrorMessage: " + MESSAGE_NUMBER_PARSE_ERROR;
		} catch(ParseException e) {
			e.printStackTrace();
			return "ErrorCode: " + CODE_DATE_PARSE_ERROR + " ErrorMessage: " + MESSAGE_DATE_PARSE_ERROR;
		} catch(Exception e) {
			e.printStackTrace();
			return "ErrorCode: " + CODE_UNKOWN_ERROR + " ErrorMessage: " + MESSAGE_UNKOWN_ERROR;
		}
	}

	
	

	public boolean saveClientIpAndDeviceId(String ip, String deviceId) {
		try {
			Client client = new Client();
			client.setClientIp(client.getClientIp());
			client.setClientIp(ip);
			client.setDeviceId(deviceId);
			client.setLastAction(new Date());
			repoClients.save(client);
			repoClients.flush();
			return true;
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public String removeClientIpAndDeviceId(String ip, String deviceId) {
		try {
			Client client = new Client();
			client.setClientIp(client.getClientIp());
			client.setClientIp(ip);
			client.setDeviceId(deviceId);
			repoClients.delete(client);
			repoClients.flush();
			return "0 - Succeed";
		} catch(Exception e) {
			e.printStackTrace();
			return "-1 - Failed";
		}
	}

	@Override
	public boolean checkAndManageClient(HttpServletRequest request, String deviceId) {
		String ip = request.getRemoteAddr();
		List<Client> response = repoClients.findByClientIp(ip);
		if(response.size() > 0 && ! response.get(0).getDeviceId().equals(deviceId)) {
			return false;
		}
		if(response.size() == 0) {
			saveClientIpAndDeviceId(ip, deviceId);
		}
		return true;
	}
	
	public String getMyDeviceId(HttpServletRequest request) {
		String ip = request.getRemoteAddr();
		List<Client> response = repoClients.findByClientIp(ip);
		if(response.size() > 0) {
			return response.get(0).getDeviceId();
		} else {
			return "N/A";
		}
		
	}

}
