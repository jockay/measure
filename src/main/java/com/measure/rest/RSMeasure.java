package com.measure.rest;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.measure.model.RespMeasureByYear;
import com.measure.model.RespMeasureCountForYear;
import com.measure.model.RespMeasureForYearByMonths;
import com.measure.repositories.RepoMeasure;
import com.measure.service.SrvcMeasure;

@RestController
@RequestMapping("/rest")
public class RSMeasure {
	
	@Autowired 
	private SrvcMeasure srvcMeasure;
	
	@Autowired
	public RepoMeasure repoMeasure;
	
	@RequestMapping("/hello")
	public String hello() {
		return "hello";
	}
	
	@RequestMapping(value = "/getmeasurementforyear/{year}/{deviceId}", method = RequestMethod.GET)
	public String getMeasurementForYear(@PathVariable("year") String year, @PathVariable("deviceId") String deviceId, HttpServletRequest request) {
		return srvcMeasure.getSumOfMeasurementsForYearStr(request, deviceId, year);
	}
	@RequestMapping(value = "/getmeasurementforyearjson/{year}/{deviceId}", method = RequestMethod.GET)
	public RespMeasureByYear getMeasurementForYearJson(@PathVariable("year") String year, @PathVariable("deviceId") String deviceId, HttpServletRequest request) {
		return srvcMeasure.getSumOfMeasurementsForYear(request, deviceId, year);
	}
	
	@RequestMapping(value = "/getmeasurementforyearbymonths/{year}/{deviceId}", method = RequestMethod.GET)
	public String getMeasurementForYearByMonths(@PathVariable("year") String year, @PathVariable("deviceId") String deviceId, HttpServletRequest request) {
		return srvcMeasure.getMeasurementsForYearByMonthsStr(year, deviceId, request);
	}
	@RequestMapping(value = "/getmeasurementforyearbymonthsjson/{year}/{deviceId}", method = RequestMethod.GET)
	public RespMeasureForYearByMonths getMeasurementForYearByMonthsJson(@PathVariable("year") String year, @PathVariable("deviceId") String deviceId, HttpServletRequest request) {
		return srvcMeasure.getMeasurementsForYearByMonths(year, deviceId, request);
	}
	
	@RequestMapping(value = "/getmeasurementcountforyear/{year}/{deviceId}", method = RequestMethod.GET)
	public String getMeasureCountForYear(@PathVariable("year") String year, @PathVariable("deviceId") String deviceId, HttpServletRequest request) {
		return srvcMeasure.getMeasurementCountForYearStr(year, deviceId, request);
	}
	@RequestMapping(value = "/getmeasurementcountforyearjson/{year}/{deviceId}", method = RequestMethod.GET)
	public RespMeasureCountForYear getMeasureCountForYearJson(@PathVariable("year") String year, @PathVariable("deviceId") String deviceId, HttpServletRequest request) {
		return srvcMeasure.getMeasurementCountForYear(year, deviceId, request);
	}
	
	@RequestMapping(value = "/addmeasurement/{date}/{value}/{deviceId}", method = RequestMethod.GET)
	public String addMeasurement(@PathVariable("date") String date, @PathVariable("value") String value, @PathVariable("deviceId") String deviceId, HttpServletRequest request) {
		return srvcMeasure.addMeasurement(date, value, deviceId, request);
	}
	
	@RequestMapping(value = "/getmydevideid", method = RequestMethod.GET)
	public String getMyDeviceId(HttpServletRequest request) {
		return srvcMeasure.getMyDeviceId(request);
	}
	
	
//	@RequestMapping(value = "/unregisterClient/{clientIp}/{deviceId}", method = RequestMethod.GET)
//	public String unregisterClientAndDevice(@PathVariable("clientIp") String clientIp, @PathVariable("deviceId") String deviceId) {
//		return srvcMeasure.removeClientIpAndDeviceId(clientIp, deviceId);
//	}
	
}
